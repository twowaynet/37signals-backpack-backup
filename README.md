# 37signals-backpack-backup

Backup all your text, pictures, files from 37signals Backpack

## Installation

  - Install nodejs on your local machine
  - Clone this repo to your local machine
  - run `npm install`

  
## Run process

  - run `npm run start`
  
The app will listen on a specific port. Visit your local machine's IP address in your browser, on the specified port. Example, http://127.0.0.1:3000. Follow the on-screen instructions in the Web user interface.